from .train import train
from .test import test
from .play import play