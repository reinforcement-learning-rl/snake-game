import random
import pygame
from .entities import Apple, Snake
from .config import SnakeGameConfig


class SnakeGame:
    def __init__(self, grid_size = (6,6), mode = "human"):
        # Initialize the game mode and clock
        pygame.init()
        self.clock = pygame.time.Clock()
        self.config = SnakeGameConfig(grid_size)
        self.mode = mode
        
    
    def reset(self):
        # Choose a random direction
        self.direction = random.choice([self.config.UP, self.config.DOWN, self.config.LEFT, self.config.RIGHT])
        
        # Spawn snake and apple
        self.snake = Snake(self.config, self.direction)
        self.apple = Apple(self.config, self.snake)
        
        # Reset all game variables
        self.score = 0
        self.game_over = False
        self.victory = False
        
        # Reset the canvas and game window
        self.canvas = pygame.Surface((self.config.WINDOW_WIDTH, self.config.WINDOW_HEIGHT))
        self.window = None
        
        return self.get_state(), self.score, self.game_over, self.victory
    
    
    def step(self, action=None):
        # Use the clock to control FPS only if the agent is not training
        if self.mode != "train":
            self.clock.tick(self.config.FPS)
            
        # Handle key presses if a human is playing
        if self.mode == "human":
            self.handle_events()
            
        # Update the state based on the action
        self.update_state(action)
    
        return self.get_state(), self.score, self.game_over, self.victory
    
    
    def update_state(self, action):
        # Update direction based on the action
        if action is not None:
            self.update_direction([self.config.UP, self.config.DOWN, self.config.LEFT, self.config.RIGHT][action])
        
        # Move the snake and check for collisions
        self.snake.move(self.direction)
        self.check_collisions()

        # Feed the snake if it has reached the apple
        if self.snake.position == self.apple.position:
            self.feed_snake()
            
        # Check the stamina of the snake
        self.check_stamina()
    
    
    def get_state(self):
        self.fill_canvas()
        
        state = pygame.surfarray.array3d(self.canvas)
        
        # Transpose to (channels, height, width) for integration with deep learning libraries
        state = state.transpose(2, 1, 0)

        return state
    
    
    def update_direction(self, new_direction):
        # Change the snake's direction if not directly opposite to current
        if new_direction == self.config.UP and self.direction != self.config.DOWN:
            self.direction = self.config.UP
        elif new_direction == self.config.DOWN and self.direction != self.config.UP:
            self.direction = self.config.DOWN
        elif new_direction == self.config.LEFT and self.direction != self.config.RIGHT:
            self.direction = self.config.LEFT
        elif new_direction == self.config.RIGHT and self.direction != self.config.LEFT:
            self.direction = self.config.RIGHT
        
        
    def check_collisions(self):
        # Check for collisions with boundaries or itself
        if  self.snake.position[0] < 0 or self.snake.position[0] > self.config.WINDOW_WIDTH - self.config.PIXEL_SIZE:
            self.game_over = True
        elif self.snake.position[1] < 0 or self.snake.position[1] > self.config.WINDOW_HEIGHT - self.config.PIXEL_SIZE:
            self.game_over = True
        elif self.snake.position in self.snake.body_segments[1:]:
            self.game_over = True
        
        
    def feed_snake(self):
        # Increase score and grow the snake when it eats an apple
        self.score += 1
        self.snake.grow()
        self.victory = self.apple.spawn(self.snake)
        
        
    def check_stamina(self):
        if self.snake.stamina <= 0:
            self.game_over = True
    
    
    def handle_events(self):
        # Handle system events from Pygame, such as quit or key presses
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    self.reset()
                else:
                    self.handle_keys(event)
    
    
    def handle_keys(self, event):
        # Respond to key presses for manual control of the snake
        if event.key == pygame.K_UP:
            self.update_direction(self.config.UP)
        elif event.key == pygame.K_DOWN:
            self.update_direction(self.config.DOWN)
        elif event.key == pygame.K_LEFT:
            self.update_direction(self.config.LEFT)
        elif event.key == pygame.K_RIGHT:
            self.update_direction(self.config.RIGHT)
            
            
    def fill_canvas(self):
        # Add the game elements onto the canvas
        self.canvas.fill(self.config.GREY)
        self.snake.render(self.canvas)
        self.apple.render(self.canvas)
            
        
    def render(self, scale_factor=2):
        # Render the canvas onto the game window
        if self.window is None:
            self.window = pygame.display.set_mode((self.config.WINDOW_WIDTH * scale_factor, self.config.WINDOW_HEIGHT * scale_factor))
            
        scaled_canvas = pygame.transform.scale(self.canvas, (self.config.WINDOW_WIDTH * scale_factor, self.config.WINDOW_HEIGHT * scale_factor))
        self.window.blit(scaled_canvas, (0, 0))

        pygame.display.update()
        
        
    def close_window(self):
        if self.window is not None:
            pygame.display.quit()
            self.window = None
            
            
    @property
    def mode(self):
        return self._mode

    
    @mode.setter
    def mode(self, value):
        if value not in self.config.MODES:
            raise ValueError(f"Invalid mode '{value}'. Valid modes are: {self.config.MODES}")
        self._mode = value
            
            
    @property
    def victory(self):
        return self._victory


    @victory.setter
    def victory(self, is_victory):
        self._victory = is_victory
        if is_victory:
            self.game_over = True