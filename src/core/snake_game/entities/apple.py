import random
import pygame
from ..config import *


class Apple:
    def __init__(self, config, snake):
        self.config = config
        self.sprite = config.SPRITES['apple']
        
        self.spawn(snake)


    def spawn(self, snake):
        # Create a set of all possible positions
        all_positions = {(x, y)
                         for x in range(0, self.config.WINDOW_WIDTH,  self.config.PIXEL_SIZE) 
                         for y in range(0, self.config.WINDOW_HEIGHT, self.config.PIXEL_SIZE)}
        
        # Remove the positions occupied by the snake
        available_positions = all_positions - set(snake.body_segments)
        
        # Randomly choose from the available positions
        if available_positions:
            self.position = random.choice(list(available_positions))
            return False
        else:
            self.position = None
            return True


    def render(self, canvas):
        # Draw apple sprite
        if self.position is not None:
            canvas.blit(self.sprite, (self.position[0], self.position[1]))