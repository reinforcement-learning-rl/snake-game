import random
import pygame
from ..config import *


class Snake:
    def __init__(self, config, direction):
        self.config  = config
        self.sprites = config.SPRITES['snake']
        
        self.direction = direction
        
        self.spawn()
        
        
        
    def spawn(self):
        self.stamina = self.reset_stamina()
        
        self.body_segments = [(random.randrange(2 * self.config.PIXEL_SIZE, 
                                                self.config.WINDOW_WIDTH  - 2 * self.config.PIXEL_SIZE, 
                                                self.config.PIXEL_SIZE),
                               random.randrange(2 * self.config.PIXEL_SIZE,
                                                self.config.WINDOW_HEIGHT - 2 * self.config.PIXEL_SIZE,
                                                self.config.PIXEL_SIZE))]
        self.position = self.body_segments[0]
        self.body_segments.append((self.position[0] - self.direction[0], 
                                   self.position[1] - self.direction[1]))
    
    
    def move(self, direction):
        # Update direction
        self.direction = direction
        
        # Update position of snake
        current_position = self.position
        self.position = ((current_position[0] + direction[0]),
                         (current_position[1] + direction[1]))
        
        self.body_segments.insert(0, self.position)
        self.last_segment = self.body_segments.pop(-1)
        
        # Lose some stamina
        self.stamina -= 1
        
        
    def grow(self):
        self.body_segments.append(self.last_segment)
        self.stamina = self.reset_stamina()
        
        
    def reset_stamina(self):
        return 10 * (self.config.GRID_SIZE[0] * self.config.GRID_SIZE[1]) #** 2
        
        
    def render(self, canvas):
    
        rotations = {self.config.UP   : 0,
                     self.config.DOWN : 180,
                     self.config.RIGHT: -90,
                     self.config.LEFT : 90}
        
    
        for i, segment in enumerate(self.body_segments):
            sprite = self.sprites['body']
            rotation = 0

            if i == 0:
                # Head segment
                sprite = self.sprites['head']
                rotation = rotations[self.direction]
                
            elif i == len(self.body_segments) - 1:
                # Tail segment
                sprite = self.sprites['tail']
                # Determine the direction to the previous segment
                prev_segment = self.body_segments[i - 1]
                if segment[0] == prev_segment[0]:
                    rotation = rotations[self.config.UP if segment[1] > prev_segment[1] else self.config.DOWN]
                else:
                    rotation = rotations[self.config.RIGHT if segment[0] < prev_segment[0] else self.config.LEFT]
                
            else:
                # Middle body segments
                sprite = self.sprites['body']
                prev_segment = self.body_segments[i - 1]
                next_segment = self.body_segments[i + 1]

                if segment[0] == prev_segment[0] == next_segment[0]:
                    # Straight vertical body segment
                    rotation = rotations[self.config.UP if segment[1] > prev_segment[1] else self.config.DOWN]
                elif segment[1] == prev_segment[1] == next_segment[1]:
                    # Straight horizontal body segment
                    rotation = rotations[self.config.LEFT if segment[0] < prev_segment[0] else self.config.RIGHT]
                else:
                    # Angled body segment
                    sprite = self.sprites['body_angle']
                    # Determine the correct rotation for the angle
                    if segment[0] == prev_segment[0]:
                        if segment[1] > prev_segment[1]:
                            rotation = rotations[self.config.LEFT if segment[0] > next_segment[0] else self.config.UP]
                        else:
                            rotation = rotations[self.config.DOWN if segment[0] > next_segment[0] else self.config.RIGHT]
                    elif segment[1] == prev_segment[1]:
                        if segment[0] > prev_segment[0]:
                            rotation = rotations[self.config.LEFT if segment[1] > next_segment[1] else self.config.DOWN]
                        else:
                            rotation = rotations[self.config.UP if segment[1] > next_segment[1] else self.config.RIGHT]        

            rotated_sprite = pygame.transform.rotate(sprite, rotation)
            canvas.blit(rotated_sprite, (segment[0], segment[1]))