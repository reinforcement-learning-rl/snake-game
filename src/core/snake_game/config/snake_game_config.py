import os
import pygame


class SnakeGameConfig:
    def __init__(self, grid_size = (6,6)):
        # Game modes
        self.MODES = ["human", "train", "test"]

        # Grid size and window dimensions
        self.GRID_SIZE = grid_size
        self.PIXEL_SIZE = 10
        self.WINDOW_WIDTH  = self.GRID_SIZE[0] * self.PIXEL_SIZE
        self.WINDOW_HEIGHT = self.GRID_SIZE[1] * self.PIXEL_SIZE
        # GRID_SIZE = (WINDOW_WIDTH // PIXEL_SIZE, WINDOW_HEIGHT // PIXEL_SIZE)

        # Speed
        self.FPS = 20

        # Directions
        self.UP    = (0               , -self.PIXEL_SIZE)
        self.DOWN  = (0               ,  self.PIXEL_SIZE)
        self.LEFT  = (-self.PIXEL_SIZE,                0)
        self.RIGHT = ( self.PIXEL_SIZE,                0)

        # Colors
        self.WHITE = (255, 255, 255)
        self.GREEN = (  0, 255,   0)
        self.RED   = (255,   0,   0)
        self.BLACK = (  0,   0,   0)
        self.GREY  = ( 50,  50,  50)

        # Sprites
        sprites_path = os.path.join(os.path.dirname(__file__), '..', 'sprites')
        self.SPRITES = {
            'snake': {
                'head'      : pygame.image.load(os.path.join(sprites_path, 'head.png')),
                'body'      : pygame.image.load(os.path.join(sprites_path, 'body.png')),
                'body_angle': pygame.image.load(os.path.join(sprites_path, 'body_angle.png')),
                'tail'      : pygame.image.load(os.path.join(sprites_path, 'tail.png'))},
            
            'apple': pygame.image.load(os.path.join(sprites_path, 'apple.png'))
            }