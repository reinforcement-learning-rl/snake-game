import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation



class Replayer:
    def __init__(self):
        self.reset()
        
    
    def reset(self):
        self.frames = []
        
        
    def add_frame(self, frame):
        self.frames.append(frame.transpose(1, 2, 0))
        
        
    def save(self, root_path, fps=15, scale=10):
        # Generate gif
        gif, figure, ax = self._generate_gif(scale)
        
        # Get file path
        file_path = self._format_file_path(root_path)
        
        # Create directory if it doesn't exist
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        # Save gif
        gif.save(file_path, fps=fps)
        print(f"Replay saved to {file_path}")

        # Close the figure
        plt.close(figure)
        
        
    def _generate_gif(self, scale):
        # Get the pixels-per-inch setting from matplotlib
        px = 1 / plt.rcParams['figure.dpi']
        
        # Calculate the figure size in inches, applying the scale factor
        shape = self.frames[0].shape
        figsize = (shape[0] * px * scale, shape[1] * px * scale)
        
        # Initialize the figure and axes objects
        figure, ax = plt.subplots(figsize=figsize)
        plt.subplots_adjust(left=0, right=1, top=1, bottom=0, wspace=0, hspace=0)
        plt.axis('off')
        
        # Create a placeholder for the frames
        self.img = plt.imshow(self.frames[0], interpolation='nearest')
        
        # Create an animation
        gif = animation.FuncAnimation(figure, self._update_figure, frames=len(self.frames), blit=True)
        
        return gif, figure, ax
    
        
    def _update_figure(self, i):
        # Function to update figure
        self.img.set_data(self.frames[i])
        return (self.img,)
    
    
    def _format_file_path(self, root_path):
        file_path = os.path.join(root_path, 'replay.gif')
        return file_path