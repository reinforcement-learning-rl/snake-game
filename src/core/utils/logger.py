import numpy as np
import matplotlib.pyplot as plt


class Logger:
    def __init__(self):
        # Initialize history
        self.history = {
            'steps': [],
            'losses' : [],
            'EMA scores': []
        }
        
        
    def log(self, step, loss, scores):
        self.history['steps'].append(step)
        self.history['losses'].append(loss)
        self.history['EMA scores'].append(self._compute_ema(scores))
        
        
    def summary(self):
        print(f"step: {self.history['steps'][-1]}, \
                loss: {self.history['losses'][-1]:.2f}, \
                EMA score: {self.history['EMA scores'][-1]:.2f}")
    
    
    def plot(self):
        steps, losses, mean_scores = self.history.values()
        
        if not hasattr(self, "plot_initialized"):  # Check if the plot is initialized
            plt.ion()
            self.figure, self.ax = plt.subplots()
            self.ax.set_title('Training...')
            self.ax.set_xlabel('Number of steps')
            self.ax.set_ylabel('EMA score')
            self.plot_initialized = True
        
        self.ax.clear() # Clear the previous figure
        self.ax.plot(steps, mean_scores)
        
        self.figure.canvas.draw() # Display the current figure
        self.figure.canvas.flush_events()

        plt.pause(0.001)  # Pause to update the figure
        
        
    def _compute_ema(self, values, alpha=0.1):
        mean_value = np.mean([value for value in values if value is not None])
        # Initialize EMA with the first score
        if not self.history['EMA scores']:
            ema_score = mean_value
        # Compute EMA using the previous EMA and the current score
        else:
            ema_score = alpha * mean_value + (1 - alpha) * self.history['EMA scores'][-1]
        
        return ema_score