import torch
import torch.nn as nn



class Conv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding = (0,0), bias = False):
        super().__init__()
        
        self.conv = nn.Sequential(
            
            nn.Conv2d(
                in_channels = in_channels,
                out_channels = out_channels,
                kernel_size = kernel_size,
                stride = stride,
                padding = padding,
                bias = bias
                ),
            
            nn.BatchNorm2d(out_channels),
            
            nn.ReLU(inplace = True),
        )
        
    
    def forward(self, x):
        return self.conv(x)
    
    
    

class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None):
        super().__init__()
        
        if not mid_channels:
            mid_channels = out_channels
            
        self.double_conv = nn.Sequential(
            
            Conv(
                in_channels = in_channels, 
                out_channels = mid_channels,
                kernel_size = (3,3),
                stride = (1,1)
                ),
            
            Conv(
                in_channels = mid_channels, 
                out_channels = out_channels,
                kernel_size = (3,3),
                stride = (1,1)
                ),
            
            nn.MaxPool2d(
                kernel_size = (2,2),
                stride = (2,2)
                )
        )
        

    def forward(self, x):
        return self.double_conv(x)
    
    
    
    
class InceptionBlock(nn.Module):
    def __init__(self, input_size):
        super().__init__()
        
        self.branch1 = nn.Sequential(
            Conv(
                in_channels  = input_size, 
                out_channels = 32, 
                kernel_size  = (1,1), 
                stride       = (1,1), 
                padding      = (0,0)
            )
        )
        
        self.branch2 = nn.Sequential(
            Conv(
                in_channels  = input_size,
                out_channels = 16,
                kernel_size  = (1,1),
                stride       = (1,1),
                padding      = (0,0)
            ),
            Conv(
                in_channels  = 16,
                out_channels = 16,
                kernel_size  = (3,3),
                stride       = (1,1),
                padding      = (1,1)
            )
        )
        
        self.branch3 = nn.Sequential(
            Conv(
                in_channels  = input_size,
                out_channels = 16,
                kernel_size  = (1,1),
                stride       = (1,1),
                padding      = (0,0)
            ),
            Conv(
                in_channels  = 16,
                out_channels = 32,
                kernel_size  = (5,5),
                stride       = (1,1),
                padding      = (2,2)
            )
        )
        
        self.branch4 = nn.Sequential(
            nn.MaxPool2d(
                kernel_size  = (3,3),
                stride       = (1,1),
                padding      = (1,1)
            ),
            Conv(
                in_channels  = input_size,
                out_channels = 16,
                kernel_size  = (1,1),
                stride       = (1,1),
                padding      = (0,0)
            )
        )
        
        
    def forward(self, x):
        return torch.cat([self.branch1(x), self.branch2(x), self.branch3(x), self.branch4(x)], 1)