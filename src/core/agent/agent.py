import random
import torch
from collections import deque



class Agent:
    def __init__(self, model, checkpoint=None, num_envs=1, max_memory=128):
        # Policy network
        self.model = model
        
        # Load checkpoint if provided
        if checkpoint:
            self.model.load_state_dict(torch.load(checkpoint))
        
        # Initialize memories
        self.memories = [Memory(max_memory) for _ in range(num_envs)]
        
        
    def get_action(self, state, deterministic=False):
        action_logits, value = self.predict(state)
        
        if deterministic:
            # Choose the action with the highest logit value
            action = torch.argmax(action_logits, dim=-1).tolist()
        else:
            # Sample an action using the distribution
            action = torch.distributions.Categorical(logits = action_logits).sample().tolist()
        
        return action, action_logits, value
        
    
    def predict(self, state):
        state = self._preprocess_state(state).to(self._get_device())
        action_logits, value = self.model(state)
        # actions_distributions = torch.distributions.Categorical(logits = actions_logits)
        return action_logits, value
        
    
    def update_memory(self, samples):
        [memory.append(sample) for memory, sample in zip(self.memories, samples)]
               
               
    def _preprocess_state(self, state):
        # TODO: Add batch dimension if the state is just one observation
        return torch.tensor(state, dtype=torch.float) / 255
    
    
    def _get_device(self):
        return next(self.model.parameters()).device
    
    
class Memory:
    def __init__(self, max_memory):
        self.memory = deque(maxlen = max_memory)
    
    
    def append(self, sample):
        state, next_state, action, action_logits, value, reward, game_over = sample
        preprocessed_sample = self._preprocess_data(state, next_state, action, action_logits, value, reward, game_over)
        self.memory.append(preprocessed_sample)
        
        
    def unpack(self):
        states, next_states, actions, actions_logits, values, rewards, game_overs = zip(*self.memory)
        
        states_tensor         = torch.stack(states)
        next_states_tensor    = torch.stack(next_states)
        actions_tensor        = torch.stack(actions)
        actions_logits_tensor = torch.stack(actions_logits)
        values_tensor         = torch.stack(values)
        rewards_tensor        = torch.stack(rewards)
        game_overs_tensor     = torch.stack(game_overs)
        
        return states_tensor, next_states_tensor, actions_tensor, actions_logits_tensor, \
               values_tensor, rewards_tensor, game_overs_tensor
        
        
    def _preprocess_data(self, state, next_state, action, action_logits, value, reward, game_over):
        # Convert data to tensors
        state_tensor      = torch.tensor(state, dtype=torch.float)
        next_state_tensor = torch.tensor(next_state, dtype=torch.float)
        action_tensor     = torch.tensor(action, dtype=torch.uint8)
        reward_tensor     = torch.tensor(reward, dtype=torch.float)
        game_over_tensor  = torch.tensor(game_over, dtype=torch.bool)

        return (state_tensor, next_state_tensor, action_tensor, action_logits, value, reward_tensor, game_over_tensor)