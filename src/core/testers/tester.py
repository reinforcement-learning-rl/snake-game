import numpy as np
import torch
from ..utils import Replayer


class Tester:
    def __init__(self, agent, env):
        self.agent = agent
        self.env = env
        self.replayer = Replayer()



    def test(self, num_games=1, save_replay=False, replay_save_path='./outputs/replays'):
        # Set the model in evaluation mode
        self.agent.model.eval()
        with torch.no_grad():
            
            score = 0
            
            for _ in range(num_games):
                score += self.play_game()
                
                # save replay as gif
                if save_replay:
                    self.replayer.save(replay_save_path)
                    self.replayer.reset()
                    
            
            print(f'Mean test score: {score/num_games}')
        
        
        
    def play_game(self):            
        # 1: Initialize environments
        next_state = self.env.reset()
        
        # 2 Store state for replay
        self.replayer.add_frame(next_state)

        while True:
            # 3: Render the environment
            self.env.render()
            
            # 4: Update states
            state = np.expand_dims(next_state, axis=0)
            
            # 5: Get the action from the agent
            action, _, _ = self.agent.get_action(state, deterministic=True)

            # 6: Perform action in the snake game
            next_state, reward, game_over, info = self.env.step(action[0])
            
            # 7: Store state for replay
            self.replayer.add_frame(next_state)
            
            # 8: Store score if the game is over and end the game
            if game_over:
                self.env.render()
                score = info["score"]
                break
            
        # 9: Close render window
        self.env.close_render()
                    
        return score