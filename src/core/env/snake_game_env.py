import time
import gym
import numpy as np
from ..snake_game import SnakeGame



class SnakeGameEnv(gym.Env):
    def __init__(self, grid_size = (6,6), mode = "test", verbose = False):
        super().__init__()
        # Initialize the Snake game
        self.snake_game = SnakeGame(grid_size, mode)
        
        # Set verbosity
        self.verbose = verbose
        
        # Define the action space (4 actions: UP, DOWN, LEFT, RIGHT)
        self.action_space = gym.spaces.Discrete(4)
        
        # Define the observation space based on the game state dimensions
        self.observation_space = gym.spaces.Box(
                low = 0,
                high = 255,
                shape = (3, self.snake_game.config.WINDOW_HEIGHT, self.snake_game.config.WINDOW_WIDTH),
                dtype = np.uint8
            )
        

    
    def reset(self):
        if self.verbose:
            print('Starting new game...')
        self.state, self.score, self.game_over, self.victory = self.snake_game.reset()
        self.reward = 0
        return self.state
        
        
    def step(self, action):
        # Apply the action to the game and get the updated state, score, and game over flag
        self.state, new_score, self.game_over, self.victory = self.snake_game.step(action)

        # Calculate the reward
        self.calculate_reward(new_score)
        
        if self.verbose:
            print(f"Action: {action}, Reward: {self.reward}, Done: {self.game_over}")
        
        return self.state, self.reward, self.game_over, {'score': new_score}
        
    
    def calculate_reward(self, new_score):
        # Reward for eating an apple
        if new_score > self.score:
            self.reward = 1
            
        # Punish taking a step without eating an apple
        else:
            self.reward = -0.001

        # Reward for winning the game
        if self.victory:
            self.reward = 1
        
        # Heavy penalty for dying
        elif self.game_over:
            self.reward = -1
        
        self.score = new_score

        
        
    def render(self):
        self.snake_game.render()
            
            
    def close_render(self):
        time.sleep(0.5)
        self.snake_game.close_window()
        
        
    # @property
    # def game_over(self):
    #     return self._game_over


    # @game_over.setter
    # def game_over(self, is_game_over):
    #     self._game_over = is_game_over
    #     if is_game_over:
    #         self.close_render()