from .env import SnakeGameEnv
from .agent import Agent
from .models import PolicyNetwork
from .trainers import A2CTrainer
from .testers import Tester



def train(config):
    # Read parameters
    GRID_SIZE = config['grid_size']
    NUM_ENVS = config['num_envs']
    ROLLOUTS = config['rollouts']
    EPOCHS = config['epochs']
    STEPS_PER_EPOCH = config['steps_per_epoch']
    LR = config['lr']
    GAMMA = config['gamma']
    INITIAL_STEP = config['initial_step']
    CHECKPOINT = config['checkpoint']
    SUMMARY_FREQ = config['summary_freq']
    PLOT_FREQ = config['plot_freq']
    SAVE_FREQ = config['save_freq']
    CHECKPOINTS_SAVE_PATH = "./outputs/checkpoints"
    
    # Initialize the environments
    train_envs = [SnakeGameEnv(grid_size=GRID_SIZE, mode="train") for _ in range(NUM_ENVS)]
    test_env   =  SnakeGameEnv(grid_size=GRID_SIZE, mode="test")
    
    # Initialize the model
    model = PolicyNetwork(obs_shape   = train_envs[0].observation_space.shape, 
                          num_actions = train_envs[0].action_space.n)
    
    # Initialize the agent
    agent = Agent(model, checkpoint=CHECKPOINT, num_envs=NUM_ENVS, max_memory=ROLLOUTS)
    
    # Initialize the trainer
    trainer = A2CTrainer(agent, train_envs, initial_step=INITIAL_STEP, lr=LR, gamma=GAMMA)
    
    # Initialize the tester
    tester = Tester(agent, test_env)
    
    # Test random agent
    tester.test(num_games=1)

    # Train agent
    for epoch in range(EPOCHS):
        
        # Train one epoch
        trainer.train_epoch(
            rollouts=ROLLOUTS, 
            steps_per_epoch=STEPS_PER_EPOCH,
            summary_freq=SUMMARY_FREQ,
            plot_freq=PLOT_FREQ,
            save_freq=SAVE_FREQ, 
            checkpoints_save_path=CHECKPOINTS_SAVE_PATH)
        
        # Test the model
        tester.test(num_games=5)