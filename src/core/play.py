from .snake_game import SnakeGame



def play(config):
    # Read parameters
    GRID_SIZE = config['grid_size']
    
    # Initialize game
    game = SnakeGame(grid_size=GRID_SIZE, mode="human")
    
    # Get initial state
    state, _, game_over, victory = game.reset()

    # Step until game is over
    while not game_over:
        state, _, game_over, victory = game.step()
        game.render()