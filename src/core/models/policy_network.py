import os
import torch
import torch.nn as nn
from .state_encoder import StateEncoder



class PolicyNetwork(nn.Module):
    def __init__(self, obs_shape, num_actions):
        super().__init__()
        
        # Get the encoded state
        self.state_encoder = StateEncoder(obs_shape, 512)

        # Fully connected layers for action probabilities
        self.actions = nn.Sequential(
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, num_actions)
        )

        # Fully connected layers for value estimation
        self.value = nn.Sequential(
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 1)
        )


    def forward(self, x):
        # Convolutional layers
        encoded_state = self.state_encoder(x)

        # Action probabilities
        actions = self.actions(encoded_state)

        # Value estimation
        value = self.value(encoded_state).squeeze(-1)

        return actions, value
    
    
    def save(self, file_path):
        # Create directory if it doesn't exist
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        # Save the state dictionary
        torch.save(self.state_dict(), file_path)
        print(f"Model saved to {file_path}")