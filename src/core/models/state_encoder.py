import torch
import torch.nn as nn
import numpy as np
from ..utils.custom_layers import Conv, InceptionBlock



class StateEncoder(nn.Module):
    def __init__(self, input_shape, output_size):
        super().__init__()
    
        self.cnn_block = nn.Sequential(
            Conv(
                in_channels  = input_shape[0],
                out_channels = 32,
                kernel_size  = (8,8),
                stride       = (4,4),
                padding      = (0,0)
            ),
            Conv(
                in_channels  = 32,
                out_channels = 64,
                kernel_size  = (4,4),
                stride       = (2,2),
                padding      = (0,0)
            ),
            Conv(
                in_channels  = 64,
                out_channels = 64,
                kernel_size  = (3,3),
                stride       = (1,1),
                padding      = (0,0)
            ),
            nn.Flatten()
        )

        cnn_output_size = self._get_cnn_output_size(input_shape)
        
        # Fully connected layers
        self.linear = nn.Sequential(
            nn.Linear(cnn_output_size, output_size),
            nn.ReLU()
        ) 


    def forward(self, x):
        x = self.cnn_block(x)
        x = self.linear(x)
        return x


    def _get_cnn_output_size(self, input_shape):
        # Mock data to compute shape
        with torch.no_grad():
            mock_data = torch.zeros((1,) + input_shape)
            mock_data = self.cnn_block(mock_data)
            return int(np.prod(mock_data.size()))