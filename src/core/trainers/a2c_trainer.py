import os
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from ..utils import Logger


class A2CTrainer:
    def __init__(self, agent, envs, initial_step=0, lr=0.001, gamma=0.99):
        
        # Agent and environments
        self.agent = agent
        self.envs = envs
        
        # Initial step for resuming training
        self.step = initial_step
        
        # Training process parameters
        self.lr = lr
        self.gamma = gamma
        
        # Loss coefficients
        self.action_loss_coef = 1.0
        self.entropy_loss_coef = 0.05
        self.value_loss_coef = 0.5
        
        # Optimizer
        self.grad_clip = 0.5
        self.optimizer = optim.Adam(self.agent.model.parameters(), lr=self.lr)
        self.criterion = nn.MSELoss()
        
        # Metrics
        self.logger = Logger()
        self.scores = [0 for _ in self.envs]
        self.num_games = [0 for _ in self.envs]
        
        # Device
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.agent.model.to(self.device)
        
        
        
    def train_epoch(self, rollouts=64, steps_per_epoch=100_000, summary_freq=100, plot_freq=500, save_freq=100_000, checkpoints_save_path='./outputs/checkpoints'):
        # 0: Set the agent in training mode
        self.agent.model.train()
         
        # 1: Parameters definition
        ROLLOUTS = rollouts                                                   # Training step frequency
        MAX_STEP = self.step + steps_per_epoch                                # Steps until end of epoch
        SUMMARY_FREQ = summary_freq if summary_freq > rollouts else rollouts  # Summary frequency
        PLOT_FREQ = plot_freq                                                 # Plotting frequency
        SAVE_FREQ = save_freq                                                 # Model checkpointing frequency
        CHECKPOINTS_SAVE_PATH = checkpoints_save_path                         # Model checkpointing path

        # 2: Initialize environments
        next_states = np.stack([env.reset() for env in self.envs])
        
        while self.step < MAX_STEP:
            self.step += 1
            
            # 3: Update states
            states = next_states
            
            # 4: Get the actions from the agent
            actions, actions_logits, values = self.agent.get_action(states)

            # 5: Perform action in the snake game
            next_states, rewards, game_overs, infos =  map(np.stack, zip(*[env.step(action) for env, action in zip(self.envs, actions)]))
        
            # 6: Store result of action in the memory of each agent
            self.agent.update_memory([(state, next_state, action, action_logits, value, reward, game_over) 
                for state, next_state, action, action_logits, value, reward, game_over \
                in zip(states, next_states, actions, actions_logits, values, rewards, game_overs)])
            
            # 7: Reset environments that are game over
            for i, (env, game_over, info) in enumerate(zip(self.envs, game_overs, infos)):
                if game_over:
                    # Save the achieved score and update number of games
                    self.scores[i] = info["score"]
                    self.num_games[i] += 1

                    # Reset environment
                    next_states[i] = env.reset()
            
            # 8: Perform one training step and log metrics
            if self.step % ROLLOUTS == 0:
                loss_dict = self.train_step()
                # Compute metrics and update history
                self.logger.log(self.step, loss_dict["total loss"], self.scores)
                
            # 9: Print summary
            if self.step % SUMMARY_FREQ == 0:
                self.logger.summary()
                
            # 10: Plot history
            if self.step % PLOT_FREQ == 0:
                self.logger.plot()
            
            # 11: Save model
            if self.step % SAVE_FREQ == 0:
                self.save_model(CHECKPOINTS_SAVE_PATH)
                    
        return self.logger
    
    
    
    def train_step(self):
        actions_list, actions_logits_list, values_list, returns_list, advantages_list = [], [], [], [], []
        
        for memory in self.agent.memories:
            # 1: Unpack the memory
            _, _, actions, actions_logits, values, rewards, game_overs = memory.unpack()
            
            # 2: Calculate returns and advantages
            returns = self.calculate_returns(values, rewards, game_overs)
            advantages = self.calculate_advantages(returns, values)
            
            # 3: Append to lists
            actions_list.append(actions)
            actions_logits_list.append(actions_logits)
            values_list.append(values)
            returns_list.append(returns)
            advantages_list.append(advantages)
           
        # 4: Convert into tensors 
        actions_batch = torch.cat(actions_list).to(self.device)
        actions_logits_batch = torch.cat(actions_logits_list).to(self.device)
        values_batch = torch.cat(values_list).to(self.device)
        returns_batch = torch.cat(returns_list).to(self.device)
        advantages_batch = torch.cat(advantages_list).to(self.device)
        
        # 5: Compute loss and perform gradient descent with the optimizer 
        loss_dict = self.optimizer_step(actions_batch, actions_logits_batch, values_batch, returns_batch, advantages_batch)
        
        return loss_dict
        
    
    
    def optimizer_step(self, actions_batch, actions_logits_batch, values_batch, returns_batch, advantages_batch):
        # 1: Obtain the distributions
        actions_distributions = torch.distributions.Categorical(logits = actions_logits_batch)
        
        # 2: Calculate losses
        # Action loss: maximize probabilities of actions that give high advantage
        action_loss = - (actions_distributions.log_prob(actions_batch) * advantages_batch).mean()
        
        # Entropy loss: encourage exploration
        entropy_loss = - (actions_distributions.entropy()).mean()
        
        # Value loss: penalize for innacurate value estimation
        value_loss = self.criterion(values_batch, returns_batch)
        
        # Total loss: add all losses together
        total_loss = self.action_loss_coef  * action_loss  + \
                     self.entropy_loss_coef * entropy_loss + \
                     self.value_loss_coef   * value_loss
        
        # 3: Perform gradient descent
        self.optimizer.zero_grad()
        total_loss.backward()
        torch.nn.utils.clip_grad_norm_(self.agent.model.parameters(), self.grad_clip)
        self.optimizer.step()
        
        return {'action loss': action_loss.item(), 
                'value loss': value_loss.item(),
                'entropy loss': entropy_loss.item(),
                'total loss': total_loss.item()}
        
    

    def calculate_returns(self, value_batch, reward_batch, game_over_batch):
        R = value_batch[-1] if not game_over_batch[-1] else 0
        R_batch = []
        
        for reward, game_over in zip(reversed(reward_batch), reversed(game_over_batch)):
            R = reward + (not game_over) * self.gamma * R
            R_batch.append(R)
            
        return torch.tensor(list(reversed(R_batch)), dtype=torch.float)
    
    
    
    def calculate_advantages(self, R_batch, value_batch):
        # Ensure both tensors are on the same device
        R_batch = R_batch.to(self.device)
        value_batch = value_batch.to(self.device)
        
        # Calculate advantages
        advantage_batch = R_batch - value_batch
        return advantage_batch
    
    
            
    def save_model(self, root_path):
        checkpoint_path = self._format_checkpoint_path(root_path)
        self.agent.model.save(checkpoint_path)
        
        
        
    def _format_checkpoint_path(self, root_path):
        grid_size = self.envs[0].snake_game.config.GRID_SIZE

        checkpoint_path = os.path.join(
            root_path,
            f'{grid_size[0]}x{grid_size[1]}_grid',
            f'model_{grid_size[0]}x{grid_size[1]}_{self.step}.pth')
        
        return checkpoint_path