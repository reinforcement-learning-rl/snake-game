import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np


class DQNTrainer:
    def __init__(self, model, lr, gamma):
        self.lr = lr
        self.gamma = gamma
        
        self.model = model
        
        self.optimizer = optim.Adam(model.parameters(), lr=self.lr)
        self.criterion = nn.MSELoss()
        
        

    def train_step(self, batch):
        # 1: Unpack the batch
        state_0_batch, state_1_batch, action_batch, reward_batch, game_over_batch = zip(*batch)

        # 2: Predict Q values (Q_0) with the network 
        Q_0_batch = self.predict(state_0_batch)
        
        # 3: Estimate real Q values (Q_1) using Bellman's equation
        Q_1_batch = Q_0_batch.clone()
        
        
        for idx in range(len(game_over_batch)):
            if game_over_batch[idx]:
                Q_value = reward_batch[idx]
            else:
                Q_value = reward_batch[idx] + self.gamma * torch.max(self.predict((state_1_batch[idx],)))
                
            Q_1_batch[idx][action_batch[idx]] = Q_value
            
        # 4: Perform gradient descent
        self.optimizer.zero_grad()
        
        loss = self.criterion(Q_1_batch, Q_0_batch)
        loss.backward()

        self.optimizer.step()
        
        
    
    def predict(self, state):
        state = self._preprocess_state_batch(state)
        return self.model(state)
    
    
    def _preprocess_state_batch(self, state_batch):
        # Convert each part of the state to a tensor
        images     = [state['image']     for state in state_batch]
        directions = [state['direction'] for state in state_batch]
    
        images_tensor     = torch.stack(images).type(torch.float)
        directions_tensor = torch.stack(directions).type(torch.float)
        
        return {'image': images_tensor, 'direction': directions_tensor}