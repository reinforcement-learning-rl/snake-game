from .env import SnakeGameEnv
from .agent import Agent
from .models import PolicyNetwork
from .testers import Tester



def test(config):
    # Read parameters
    GRID_SIZE = config['grid_size']
    NUM_GAMES = config['num_games']
    CHECKPOINT = config['checkpoint']
    SAVE_REPLAY = config['save_replay']
    REPLAY_SAVE_PATH = config['replay_save_path']

    # Initialize the environment
    test_env = SnakeGameEnv(grid_size=GRID_SIZE, mode="test")
    
    # Initialize the model
    model = PolicyNetwork(obs_shape = test_env.observation_space.shape, 
                          num_actions = test_env.action_space.n)
    
    # Initialize the agent
    agent = Agent(model, checkpoint=CHECKPOINT)
  
    # Initialize the tester
    tester = Tester(agent, test_env)
    
    # Test random agent
    tester.test(num_games=NUM_GAMES, save_replay=SAVE_REPLAY, replay_save_path=REPLAY_SAVE_PATH)