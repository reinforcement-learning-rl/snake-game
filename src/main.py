import argparse
import yaml
from core import train, test, play



def parse_args():
    # Create the parser
    parser = argparse.ArgumentParser()
    
    # Add arguments
    parser.add_argument(
        '--mode', 
        choices = ['train', 'test', 'play'], 
        required = True, 
        help = "Modes: train, test, play.")
    
    parser.add_argument(
        '--config',
        type = str,
        default = './config/config.yaml',
        help = 'Path to the configuration file.')
    
    # Return the parsed arguments
    return parser.parse_args()



def main():
    # Parse arguments
    args = parse_args()
    
    # Read configuration file
    with open(args.config, 'r') as file:
        config = yaml.safe_load(file)
    
    if args.mode == 'train':
        train(config['train'])
        
    elif args.mode == 'test':
        test(config['test'])
    
    elif args.mode == 'play':
        play(config['play'])
        
    else:
        print(f'Mode {args.mode} not recognized.')
        


if __name__ == "__main__":
    main()