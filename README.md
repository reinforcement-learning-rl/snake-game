<figure align="middle">
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/snake-game-assets/banner.png" alt="Banner" width="100%">
</figure>

# Python plays _Snake_
In this repository, I share my work on training a Deep Reinforcement Learning (DRL) agent to play and consistently win at the classic game of _Snake_. This is achieved while solely relying on the raw game frames for decision-making, like a human player would.

<figure align="middle">
	<img src="https://gitlab.com/all-assets/assets/-/raw/main/snake-game-assets/replay_model_10x10_100000_V1.gif" alt="Replay 1" width="25%" />
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/common-assets/transparent.png" width="3%" />
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/snake-game-assets/replay_model_10x10_500000_V1.gif" alt="Replay 2" width="25%" />
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/common-assets/transparent.png" width="3%"/>
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/snake-game-assets/replay_model_10x10_10000000_V1.gif" alt="Replay 3" width="25%" />
  <figcaption align="justify">
  Check out the autonomous agent's journey as it progresses from randomly picking actions to mastering the game of <i>Snake</i>. The AI quickly learns basic survival tactics, such as avoiding walls, and gradually develops strategic foresight that ultimately allows it to win.
  </figcaption>
</figure>

<br>

## Table of Contents
- [Key Features](#key-features)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Usage](#usage)
- [About This Project](#about-this-project)
  - [The _Snake_ Game](#the-snake-game)
  - [Why _Snake_?](#why-snake)
  - [Learning to Play as a DRL Agent — Part 1: The Basics](#learning-to-play-as-a-drl-agent-part-1-the-basics)
  - [Learning to Play as a DRL Agent — Part 2: The Algorithm](#learning-to-play-as-a-drl-agent-part-2-the-algorithm)
- [References](#references)
- [License](#license)
- [Contact](#contact)

<br>

## Key Features

<p></p>
🐍 <strong><i>Snake</i> game implementation with Pygame.</strong>

-	Supports both human and AI players, so you can try the game for yourself.
-	Configurable grid size to adjust state space dimensionality.
-	Custom sprites to give our snake a unique personality!

<p></p>
✨ <strong>Advantage Actor-Critic (A2C) algorithm implementation with PyTorch.</strong>

-	Image-based learning through Convolutional Neural Networks (CNNs).
-	Multi-environment training to encourage model generalization.
-	GPU acceleration to speed up training.
-	Deterministic mode for testing model performance.
-	Replay functionality for storing gameplays during model testing.

<p></p>
🐋 <strong>Docker integration.</strong>

-	Containerized setup for keeping things tidy.
-	X11 forwarding to display live graphical output.


<br>

## Getting Started

### Installation

1. __Clone this repository:__
    ```
    git clone https://gitlab.com/reinforcement-learning-rl/snake-game.git
    ```

2. __Access the project directory:__
    ```
    cd snake-game/
    ```

3. __Build the Docker image:__
    ```
    ./build-docker.sh
    ```

> 📌 __NOTE:__ Using Docker for setup is recommended, but not required. Alternatively, you can manually install the necessary dependencies.


### Usage

1. __Start the Docker container:__\
  Launch the Docker container using the provided script. This script mounts the project directory `src` into the container, allowing live editing of files, and ensuring that any outputs generated inside the container are accessible in your local directory. This script also sets up necessary configurations, including GPU access and X11 forwarding.
    ```
    ./run-docker.sh
    ```

2. __Modify the configuration file (optional):__\
Edit the file `./src/config/config.yaml` to modify the _Snake_ game configuration, as well as the training and testing parameters.

3. __Run the main script:__\
You can run the `main.py` script in one of the following modes:
    - __Training mode:__\
    To start training a model, run:
      ```
      python main.py --mode train
      ```
    - __Testing mode:__\
    For testing the provided pre-trained models or your own saved checkpoints, run:
      ```
      python main.py --mode test
      ```
    - __Playing mode:__\
    For playing the _Snake_ game yourself, run:
      ```
      python main.py --mode play
      ```

> 📌 __NOTE:__ If encountering display issues, you may need to adjust the `DISPLAY` variable within the `run-docker.sh` script to match your host system's X11 settings.

> 🥠 __TIP:__ Fully training a model may require long training times $($ $\sim 10^7$ steps $)$, especially for larger grid sizes.

<br>

## About This Project

### The _Snake_ Game
_Snake_ is an iconic arcade game that many of us are familiar with — due to its simple mechanics and widespread popularity it requires little introduction, so I will have it quickly summarized:

In the classic version of this game, the player controls a pixelated snake which roams within the confines of its little 2D grid-world foraging for food pellets that spawn at random locations.

The snake is in constant motion, and the player can steer its head in any[^footnote-1] of the four cardinal directions—up, down, left, or right—to guide it towards the food. As the snake moves across the grid-world, it interacts with its environment in two distinct ways:
-	Each time the snake reaches and eats a food pellet, its tail grows longer and the player's score increases by one point.
-	Conversely, if at any point the snake hits the edges of the grid-world or its own body, the game ends.

The game starts off fairly easy and forgiving, but, as the snake lengthens and starts congesting the grid, avoiding collisions becomes increasingly harder, demanding the player to think ahead to prevent the snake from becoming entrapped by its own growing tail.

The game is won when the snake grows to occupy every available space on the grid. Achieving this is exceptionally challenging, as players rarely manage to sustain the snake's survival long enough to succeed at it.

<br>

### Why _Snake_?
I have always been into video games, so it's baffling why it took me so long to combine this interest with my passion for AI, but it certainly was long overdue. Back in the day, I used to be pretty decent at _Snake_, yet I never came remotely close to winning. Now, I still can't beat this game myself, but I can train an AI to do it for me! — also, watching it play is downright hypnotizing (or is it just me?). But on a more serious note, there are also technical considerations for selecting _Snake_ as the focus of this project:

_Snake_ serves as an ideal testbed for AI algorithms due to its straightforward rules and low technical requirements, while presenting a tricky challenge due to its dynamic state space and the need for long-term strategic planning.

As a result, it has gained traction among those experimenting with autonomous decision-making algorithms. Yet, existing implementations often miss the opportunity to fully exploit the game's complexity.

Particularly, most attempts give the autonomous agent a drastically simplified view of the game by using handcrafted features[^footnote-2]. This tailored representation works as a shortcut for the agent by reducing the dimensionality of the state space it has to process; in the short term the agent can learn a lot faster, however:

1. This simplification is inherently linked to our prior knowledge of the problem's solution, and it bypasses a crucial step in the learning process — discovering and understanding the underlying structure of the problem on its own. This does not translate well to most real-world tasks, where the relevant features are often unapparent, requiring the algorithm to learn directly from raw data.

2. These representations lack the detail necessary for the agent to plan beyond the immediate future, leading to a short-sightedness that caps its performance; the agent learns to play but remains incapable of consistently winning.

This prompted me to try a different approach. Unlike these attempts, I feed the pixels from the game frames directly to the agent. This is essentially the same information a human player would receive, which—I believe—makes the learning process both more challenging and authentic.

<br>

### Learning to Play as a DRL Agent — Part 1: The Basics

Our goal is to train an autonomous agent to play _Snake_ under conditions equivalent to those a human player would face. On that account, our agent will iteratively receive the current game frame and be required to decide the direction in which the snake should move next.

Initially, the agent has no knowledge of how to play _Snake_. No rules about the game are provided, nor is any strategy explicitly taught. So, how can it be expected to improve, let alone win? This is where Reinforcement Learning (RL) comes into play.

RL is an area of Machine Learning concerned with how autonomous agents learn an optimal—or near-optimal—policy[^footnote-3] through trial-and-error interactions with their environment, using feedback in the form of rewards or penalties to guide their learning process.

To implement this approach, our problem needs to be formalized as a Markov Decision Process (MDP). This entails expressing the _Snake_ game in terms of the following components:

-	__The *state space*__ ($S$), which is the set of all possible states the agent can encounter;
-	__The *action space*__ ($A$), the set of all possible actions available to the agent in any given state;
-	__A *reward function*__ ($R$), which countifies the "desirability" of an outcome;
-	__A *transition probability matrix*__ ($P$), which describes the transitions from one state to another.

Although this might seem a bit confusing at first, it is actually quite straightforward in the context of our problem:

1. __The *state space*__ ($S$)\
    In the RL terminology, a _state_ refers to a mathematical representation of the circumstances in which the agent finds itself. While there are many valid representations, in this case I specifically wanted to use the RGB image depicting the current frame of the game screen. Thus, the _state space_ encompasses all possible RGB images of the game screen that the agent can observe.

2. __The *action space*__ ($A$)\
    A human player can control the snake using the arrow keys on a keyboard. However, our AI agent—lacking this capability—will instead output an index to signal the direction in which it wants the snake to move:

    `{0: up, 1: down, 2: right, 3: left}`

    In the RL terminology, this output is referred to as the _action_, and the set of these four possible actions constitutes our _action space_.

3. __The *reward function*__ ($R$)\
    The _reward_ is a numeric signal that serves as feedback for the agent to guide its learning process, so it's crucial to design a reward function that aligns with the objective of the task at hand.

    In theory, this seems straightforward: reward desirable behaviors—such as surviving and eating food pellets—to encourage them, and penalize undesirable ones—like crashing into an obstacle. However, RL agents can be surprisingly good at discovering loopholes in the reward function that allow them to achieve a high reward, even if the resulting strategy is either absurd or outright counterproductive from a human perspective.

    For instance, rewarding the agent for staying alive might lead it to exploit the situation by endlessly running in a circle rather than seeking food (maybe it's on to something?). Conversely, if we try to avoid this behavior by harshly punishing it for anything other than reaching the food, this could result in a self-destructive agent that rushes into the nearest wall upon spawning, concluding that a quick game over is—mathematically—preferable to enduring the punitive conditions of its existence.

    While harmless and amusing in the context of a video game, this points to a broader issue of AI: the lack "common sense". However, that discussion is for another time. The main idea I want to get across is that designing a reward function that captures exactly what you want the agent to do is far from trivial.

    In this case, the following reward function proved effective:

    <table align="center">
      <tr>
        <th> Condition (evaluated at each step)</th>
        <th> Reward </th>
      </tr>
      <tr>
        <td> Nothing happened </td>
        <td align="center"> -0.001 </td>
      </tr>
      <tr>
        <td> Ate food pellet </td>
        <td align="center"> +1 </td>
      </tr>
      <tr>
        <td> Victory </td>
        <td align="center"> +1 </td>
      </tr>
      <tr>
        <td> Game over </td>
        <td align="center"> -1 </td>
      </tr>
    </table>

4. __The *transition probability matrix*__ ($P$)\
    The _transition probability matrix_ outlines the probabilities of moving from one _state_ to another following a specific _action_.

    Given the _Snake_ game's combination of deterministic elements—where the snake's direction unequivocally decides its next position—and stochastic elements—such as the random placement of food pellets—detailing these probabilities for every possible scenario is impractical.

    In this case, rather than explicitly defining a probability matrix, the state transition dynamics can be implicitly determined by the game's inherent mechanics.

<br>

### Learning to Play as a DRL Agent — Part 2: The Algorithm

Of course, for the magic of training to unfold, all components of the MDP must be orchestrated through the application of RL. The landscape of modern RL algorithms is broad and diverse, with each offering its own set of strengths and trade-offs, making their suitability dependent on the nature of the problem at hand. In this case, I opted for the __*Advantage Actor-Critic* (A2C) algorithm__. For those who are unfamiliar with this algorithm, here is a very concise overview focused on its application to this project. If you are interested in delving deeper into the technical intricacies of A2C, do check out the original research paper [1].

The A2C algorithm operates on the principle of dividing responsibilities — its decision-making mechanism is made of two components: the _actor_ and the _critic_. Both elements receive the current _state_ as input, but serve distinct roles:

-	__The *Actor*__ is responsible for deciding which _action_ to take in the given _state_; for instance, in our context, it dictates the direction for the snake to move.

-	__The *Critic*__ estimates the _value_ of the given _state_; it essentially evaluates how beneficial the current situation is by predicting the expected _return_ (the cumulative future rewards) from that point onwards.

Both the _actor_ and the _critic_ are modeled using neural networks, implemented as two separate heads that branch off from a shared backbone. This backbone employs Convolutional Neural Networks (CNNs) to interpret the pixels—effectively "seeing" what is happening—on the game screen, providing both the _actor_ and the _critic_ with a common understanding of the _state_. The shared output of the backbone is then further processed by each component's head according to its distinct purpose.

The suitability of the A2C algorithm for the game of _Snake_ has a lot to do with a central challenge in decision-making theories known as the exploration-exploitation dilemma.

-	__Exploring__ involves trying out different actions to gather more information about the environment, which is essential for discovering more effective strategies.

-	__Exploiting__ means selecting the best action based on what has previously worked well, which is necessary for achieving a high score.

Traditional exploration methods[^footnote-4] often address this dilemma by allowing the agent to act deterministically (exploit) most of the time, while incorporating a certain probability for selecting random actions (explore). The value of this probability is what controls the balance between exploration and exploitation within these strategies.

In the _Snake_ game, where wrong moves can quickly end the game, blindly relying on random actions for exploring can be... problematic — opting for a high exploration rate will likely result in frequent game overs, denying the agent the opportunity to progress and learn from the game's later stages; conversely, a low exploration rate is safer but can prevent the agent from discovering potentially superior strategies.

Contrary to these methods, which fully rely on chance to dictate when and which actions to explore, A2C's exploration mechanism is tied to the agent's confidence in its decisions. Rather than alternating between deterministic and random decision-making, the agent always chooses its _action_ by sampling from a probability distribution across the _action space_ that is learned by the _actor_ head during training.

Early in the learning process, when less is known, this probability distribution encourages trying a variety of actions. As the agent accumulates experience, the distribution becomes more skewed, reflecting the agent's growing confidence in its strategy.

For example, consider the following _state_ of our _Snake_ environment:

<figure align="middle">
  <img src="https://gitlab.com/all-assets/assets/-/raw/main/snake-game-assets/game_frame.png" alt="Game frame" width="25%">
</figure>

After undergoing some training, the agent's _actor_ head may output an array such as: `[0.43, 0.08, 0.47, 0.02]`, where each element represents the likelihood of moving `up`, `down`, `right`, and `left`, respectively.

From this distribution:

-	The significantly lower probabilities assigned to `down` (0.08) and `left` (0.02) suggest the agent is confident that moving in these directions will not lead to a favorable outcome in the current scenario, resulting in a negligible chance of selecting these actions. By doing so, the agent avoids repeating the same mistakes over and over.

-	The higher probabilities for `up` (0.43) and `right` (0.47) imply that the agent considers these actions as promising but remains unsure which will work better. This results in a similar chance of trying each, allowing for exploration to uncover the optimal strategy.

I hope this example has been helpful in illustrating why A2C is an ideal option for training agents in environments like _Snake_, where learning through exploration is essential, yet the cost of mistakes is high.


<br>

## References

<a id="reference-1">[1]</a> 
Mnih et al, "Asynchronous Methods for Deep Reinforcement Learning," 2016, https://arxiv.org/abs/1602.01783


<a id="reference-2">[2]</a> 
Patrick Loeber, "Teach AI To Play Snake! Reinforcement Learning With PyTorch and Pygame", 2021, GitHub repository, https://github.com/patrickloeber/snake-ai-pytorch

<a id="reference-3">[3]</a> 
Code Bullet, "A.I. Learns to play Snake using Deep Q Learning", 2020, YouTube video, https://www.youtube.com/watch?v=-NJ9frfAWRo

<a id="reference-4">[4]</a> 
Harrison Kinsley, "Custom Environments - Reinforcement Learning with Stable Baselines 3 (P.3)", 2022, YouTube video, https://www.youtube.com/watch?v=uKnjGn8fF70

<a id="reference-5">[5]</a> 
Aleksei Petrenko, "snake-rl", 2019, GitHub Repository, https://github.com/alex-petrenko/snake-rl

<br>

## License

Distributed under the MIT License. See [`LICENSE`](https://gitlab.com/reinforcement-learning-rl/snake-game/-/blob/main/LICENSE "MIT License") for more information.

<br>

## Contact

**Inés Vargas Torres** · [`📫 Email`](ines.vargas.torres@gmail.com "ines.vargas.torres@gmail.com")
[`🦊 GitLab`](https://gitlab.com/ines-vargas "https://gitlab.com/ines-vargas")
[`💼 LinkedIn`](https://www.linkedin.com/in/ines-vargas-torres/ "https://www.linkedin.com/in/ines-vargas-torres/")

<br>

## Footnotes

[^footnote-1]: Excluding the one diametrically opposed to the snake's current heading, since directly reversing its movement is not allowed.

[^footnote-2]: The term "handcrafted features" refers to specifically designed variables that simplify the game's state for the AI. An example of this approach can be found in Patrick Loeber's work [[2]](#reference-2), where he represents the game state as an array of 11 boolean values. These values indicate immediate threats and directional information regarding both the snake and food location: `[danger up, danger right, danger left, direction up, direction down, direction right, direction left, food up, food down, food right, food left]`.

[^footnote-3]: In the Reinforcement Learning terminology, the strategy used by the agent to make decisions is referred to as a policy. On a theoretical level, a policy, $\pi$, is a mapping from the state space to the action space, often represented as a probability distribution.

[^footnote-4]: This refers specifically to the $\varepsilon$-greedy exploration strategy, which is one of the simplest, yet most prevalent, approaches used in RL algorithms.