#!/bin/bash

######################################
# Set container and image names
######################################
CONTAINER_NAME="snake-game-cont"
IMAGE_NAME="snake-game-img"

######################################
# X11 variables
######################################
DISPLAY=${DISPLAY:-localhost:10.0} # Set default DISPLAY if not set
export DISPLAY

XAUTHORITY=${XAUTHORITY:-~/.Xauthority} # Set default XAUTHORITY if not set
export XAUTHORITY

######################################
# Run docker container
######################################
docker run \
    -it \
    --rm \
    --gpus all \
    --name $CONTAINER_NAME \
    --net host --ipc host --shm-size=1g\
    --ulimit memlock=-1 --ulimit stack=67108864 \
    -e DISPLAY=$DISPLAY \
    -e XAUTHORITY=/.Xauthority \
    -v $XAUTHORITY:/.Xauthority \
    -v "$(pwd)"/src:/app/src \
    $IMAGE_NAME